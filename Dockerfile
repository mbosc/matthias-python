FROM python:3.8-alpine

COPY . /scripts/

WORKDIR /scripts/

RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["python", "app.py"]
